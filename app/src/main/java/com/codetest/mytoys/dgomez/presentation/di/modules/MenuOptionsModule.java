package com.codetest.mytoys.dgomez.presentation.di.modules;

import com.codetest.mytoys.dgomez.domain.interactors.menu.GetMenuOptionsInteractor;
import com.codetest.mytoys.dgomez.domain.interactors.menu.GetMenuOptionsInteractorImpl;
import com.codetest.mytoys.dgomez.presentation.presenter.MainPresenter;
import com.codetest.mytoys.dgomez.presentation.presenter.MainPresenterImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Dependencies for menu options use case.
 * @author Débora Gómez Bertoli.
 */
@Module
public class MenuOptionsModule {


    @Provides
    public MainPresenter providePresenter(MainPresenterImpl presenter){
        return presenter;
    }

    @Provides
    public GetMenuOptionsInteractor providesInteractor(GetMenuOptionsInteractorImpl interactor){
        return interactor;
    }

}
