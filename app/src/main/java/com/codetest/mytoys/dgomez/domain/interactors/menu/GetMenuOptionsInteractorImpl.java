package com.codetest.mytoys.dgomez.domain.interactors.menu;

import javax.inject.Inject;

import android.support.annotation.NonNull;

import com.codetest.mytoys.dgomez.domain.executors.PostExecutionThread;
import com.codetest.mytoys.dgomez.domain.executors.ThreadExecutor;
import com.codetest.mytoys.dgomez.domain.interactors.BaseInteractor;
import com.codetest.mytoys.dgomez.domain.model.NavigationEntries;
import com.codetest.mytoys.dgomez.domain.repository.MenuOptionsRepository;

/**
 * Interactor implementation for menu options.
 * @author Débora Gómez Bertoli.
 */
public class GetMenuOptionsInteractorImpl extends BaseInteractor implements GetMenuOptionsInteractor {

    private MenuOptionsRepository repository;

    private Callback callback;

    /**
     * Constructor.
     * @param threadExecutor The thread executor.
     * @param postExecutionThread The post execution thread.
     * @param repository The menu options repository.
     */
    @Inject
    public GetMenuOptionsInteractorImpl(@NonNull ThreadExecutor threadExecutor, @NonNull PostExecutionThread postExecutionThread, @NonNull MenuOptionsRepository repository) {
        super(threadExecutor, postExecutionThread);
        this.repository = repository;
    }

    @Override
    public void execute(@NonNull Callback callback) {
        this.callback = callback;
        execute();
    }

    @Override
    public void run() {

        repository.getMenuEntries(new MenuOptionsRepository.Callback() {
            @Override
            public void onSuccess(@NonNull final NavigationEntries entries) {

                post(new Runnable() {
                    @Override
                    public void run() {
                        callback.onSuccess(entries);
                    }
                });

            }

            @Override
            public void onError(@NonNull final Exception exception) {

                post(new Runnable() {
                    @Override
                    public void run() {
                        callback.onError(exception);
                    }
                });

            }
        });

    }
}
