package com.codetest.mytoys.dgomez.presentation.executor;

/**
 * @author Débora Gómez Bertoli.
 */

import javax.inject.Inject;

import android.os.Handler;
import android.os.Looper;

import com.codetest.mytoys.dgomez.domain.executors.PostExecutionThread;

/**
 * MainThread (UI Thread) implementation based on a Handler instantiated with the main application Looper.
 * @author Débora Gómez Bertoli.
 */
public class UiThread implements PostExecutionThread {

    private final Handler handler;

    /**
     * Constructor.
     */
    @Inject
    public UiThread() {
        this.handler = new Handler(Looper.getMainLooper());
    }

    /**
     * Causes the Runnable r to be added to the message queue. The runnable will be run on the main thread.
     * @param runnable {@link Runnable} to be executed.
     */
    @Override
    public void post(Runnable runnable) {
        handler.post(runnable);
    }
}
