package com.codetest.mytoys.dgomez.presentation.di.modules;

import javax.inject.Singleton;

import android.content.Context;

import com.codetest.mytoys.dgomez.data.datasource.network.retrofit.JsonService;
import com.codetest.mytoys.dgomez.data.datasource.network.retrofit.RetrofitService;
import com.codetest.mytoys.dgomez.data.executor.RequestExecutor;
import com.codetest.mytoys.dgomez.domain.executors.PostExecutionThread;
import com.codetest.mytoys.dgomez.domain.executors.ThreadExecutor;
import com.codetest.mytoys.dgomez.presentation.MyToysApplication;
import com.codetest.mytoys.dgomez.presentation.executor.UiThread;

import dagger.Module;
import dagger.Provides;

/**
 * Application cross dependencies.
 * @author Débora Gómez Bertoli.
 */
@Module(includes = {RepositoryModule.class, MenuOptionsModule.class})
public class ApplicationModule {

    private final MyToysApplication application;

    public ApplicationModule(MyToysApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    public Context provideApplicationContext() {
        return application;
    }

    @Provides
    @Singleton
    public ThreadExecutor provideJobExecutor(RequestExecutor jobExecutor) {
        return jobExecutor;
    }

    @Provides
    @Singleton
    public PostExecutionThread providePostExecutionThread(UiThread postExecutionThread) {
        return postExecutionThread;
    }

    @Provides
    @Singleton
    JsonService providesJsonParser() {
        return RetrofitService.getJsonService();
    }
}
