package com.codetest.mytoys.dgomez.presentation.presenter;

import javax.inject.Inject;

import android.support.annotation.NonNull;

import com.codetest.mytoys.dgomez.domain.interactors.menu.GetMenuOptionsInteractor;
import com.codetest.mytoys.dgomez.domain.interactors.menu.GetMenuOptionsInteractor.Callback;
import com.codetest.mytoys.dgomez.domain.model.NavigationEntries;
import com.codetest.mytoys.dgomez.presentation.view.MainView;

/**
 * Implements the presenter for the main view.
 * @author Débora Gómez Bertoli.
 */
public class MainPresenterImpl implements MainPresenter {

    private GetMenuOptionsInteractor interactor;

    private MainView view;

    /**
     * Constructor.
     * @param interactor Interactor for getting the menu options.
     */
    @Inject
    public MainPresenterImpl(@NonNull GetMenuOptionsInteractor interactor) {
        this.interactor = interactor;
    }

    @Override
    public void onCreate() {

        if (view == null) {
            return;
        }

        interactor.execute(new Callback() {
            @Override
            public void onSuccess(@NonNull NavigationEntries entries) {

                view.fillNavigationDrawer(entries);
            }

            @Override
            public void onError(@NonNull Exception e) {

                view.showError();
            }
        });
    }

    @Override
    public void setView(@NonNull MainView view) {
        this.view = view;
    }
}
