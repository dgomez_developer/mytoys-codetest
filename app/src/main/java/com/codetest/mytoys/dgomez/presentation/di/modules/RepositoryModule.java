package com.codetest.mytoys.dgomez.presentation.di.modules;

import javax.inject.Singleton;

import com.codetest.mytoys.dgomez.data.MenuOptionsRepositoryImpl;
import com.codetest.mytoys.dgomez.domain.repository.MenuOptionsRepository;

import dagger.Module;
import dagger.Provides;

/**
 * Repository dependencies.
 * @author Débora Gómez Bertoli.
 */
@Module(includes = {DataSourceModule.class})
public class RepositoryModule {


    @Provides
    @Singleton
    public MenuOptionsRepository provideRepository(MenuOptionsRepositoryImpl repository){
        return repository;
    }
}
