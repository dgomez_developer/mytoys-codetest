package com.codetest.mytoys.dgomez.data.datasource.network.retrofit;

import com.codetest.mytoys.dgomez.domain.model.NavigationEntries;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

/**
 * The retrofit JSON service.
 * @author Débora Gómez Bertoli.
 */
public interface JsonService {

    /**
     * Gets the menu entries from the server.
     * @return the request response.
     */
    @GET("/api/navigation")
    @Headers({"x-api-key: hz7JPdKK069Ui1TRxxd1k8BQcocSVDkj219DVzzD"})
    Call<NavigationEntries> getEntries();

}
