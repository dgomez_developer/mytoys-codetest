package com.codetest.mytoys.dgomez.domain.model;

import java.util.List;

/**
 * The entry type child.
 * @author Débora Gómez Bertoli.
 */
public class Child {

    private String label;

    private String type;

    private String url;

    private List<Child> children;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<Child> getChildren() {
        return children;
    }

    public void setChildren(List<Child> children) {
        this.children = children;
    }

}
