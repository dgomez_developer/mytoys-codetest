package com.codetest.mytoys.dgomez.presentation.view;

import javax.inject.Inject;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.codetest.mytoys.dgomez.R;
import com.codetest.mytoys.dgomez.domain.model.NavigationEntries;
import com.codetest.mytoys.dgomez.presentation.MyToysApplication;
import com.codetest.mytoys.dgomez.presentation.di.components.ApplicationComponent;
import com.codetest.mytoys.dgomez.presentation.presenter.MainPresenter;
import com.codetest.mytoys.dgomez.presentation.view.components.CustomNavigationView;
import com.codetest.mytoys.dgomez.presentation.view.components.CustomNavigationView.NavigationDrawerItemListener;

/**
 * Class that presents the MyToys website.
 * @author Débora Gómez Bertoli.
 */
public class MainActivity extends AppCompatActivity implements MainView, NavigationDrawerItemListener {

    private CustomNavigationView navigationView;

    private WebViewFragment webViewFragment;

    @Inject
    MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_NoActionBar);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getApplicationComponent().inject(this);

        //Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Navigation drawer
        initNavigationDrawer(toolbar);

        //Presenter
        presenter.setView(this);
        presenter.onCreate();

        //WebView
        initWebView();

    }

    private void initNavigationDrawer(Toolbar toolbar) {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (CustomNavigationView) findViewById(R.id.navigation_view);
        navigationView.setBackListener(this);
    }

    private void initWebView() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        webViewFragment = WebViewFragment.newInstance();
        transaction.replace(R.id.main_content, webViewFragment);
        transaction.commit();
    }

    private ApplicationComponent getApplicationComponent() {
        return ((MyToysApplication) getApplication()).getApplicationComponent();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void loadUrl(@NonNull String url) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        webViewFragment.loadUrl(url);
    }

    @Override
    public void fillNavigationDrawer(@NonNull NavigationEntries entries) {
        navigationView.initListViewContent(entries);
    }

    @Override
    public void showError() {
        Snackbar.make(findViewById(android.R.id.content), "ERROR", Snackbar.LENGTH_LONG).show();
    }

}
