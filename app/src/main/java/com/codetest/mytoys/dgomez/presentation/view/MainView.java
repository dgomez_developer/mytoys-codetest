package com.codetest.mytoys.dgomez.presentation.view;

import android.support.annotation.NonNull;

import com.codetest.mytoys.dgomez.domain.model.NavigationEntries;

/**
 * Interface to be implemented by @link{MainActivity.java}
 * @author Débora Gómez Bertoli.
 */
public interface MainView {

    /**
     * Fills the navigation drawer options.
     * @param items The navigation drawer items.
     */
    void fillNavigationDrawer(@NonNull NavigationEntries items);

    /**
     * Shows an error
     */
    void showError();
}
