package com.codetest.mytoys.dgomez.presentation.di.components;

import javax.inject.Singleton;

import android.content.Context;

import com.codetest.mytoys.dgomez.data.datasource.network.retrofit.JsonService;
import com.codetest.mytoys.dgomez.domain.executors.PostExecutionThread;
import com.codetest.mytoys.dgomez.domain.executors.ThreadExecutor;
import com.codetest.mytoys.dgomez.domain.repository.MenuOptionsRepository;
import com.codetest.mytoys.dgomez.presentation.MyToysApplication;
import com.codetest.mytoys.dgomez.presentation.di.modules.ApplicationModule;
import com.codetest.mytoys.dgomez.presentation.di.modules.RepositoryModule;
import com.codetest.mytoys.dgomez.presentation.view.MainActivity;

import dagger.Component;

/**
 * The application component.
 * @author Débora Gómez Bertoli.
 */
@Singleton
@Component(modules = {ApplicationModule.class, RepositoryModule.class})
public interface ApplicationComponent {

    void inject(MainActivity activity);

    void inject(MyToysApplication woodyApplication);

    Context context();

    ThreadExecutor threadExecutor();

    PostExecutionThread postExecutionThread();

    MenuOptionsRepository speciesRepository();

    JsonService jsonService();

}
