package com.codetest.mytoys.dgomez.data.executor;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import com.codetest.mytoys.dgomez.domain.executors.ThreadExecutor;

/**
 * Decorated {@link java.util.concurrent.ThreadPoolExecutor} Singleton class based on 'Initialization on Demand Holder'
 * pattern.
 *
 * @author Débora Gómez Bertoli.
 */
public class RequestExecutor implements ThreadExecutor {

    private static final int INITIAL_POOL_SIZE = 3;

    private static final int MAX_POOL_SIZE = 5;

    // Sets the amount of time an idle thread waits before terminating.
    private static final int KEEP_ALIVE_TIME = 10;

    // Sets the Time Unit to seconds.
    private static final TimeUnit KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;

    private final BlockingQueue<Runnable> workQueue;

    private final ThreadPoolExecutor threadPoolExecutor;

    /**
     * Constructor.
     */
    @Inject
    public RequestExecutor() {

        workQueue = new LinkedBlockingQueue<>();
        threadPoolExecutor = new ThreadPoolExecutor(
                INITIAL_POOL_SIZE,
                MAX_POOL_SIZE,
                KEEP_ALIVE_TIME,
                KEEP_ALIVE_TIME_UNIT,
                workQueue);
    }

    @Override
    public void execute(Runnable runnable) {
        if (runnable == null) {
            throw new IllegalArgumentException("Runnable to execute cannot be null");
        }
        threadPoolExecutor.execute(runnable);
    }

}
