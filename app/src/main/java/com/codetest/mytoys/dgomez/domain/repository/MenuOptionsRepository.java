package com.codetest.mytoys.dgomez.domain.repository;

import android.support.annotation.NonNull;

import com.codetest.mytoys.dgomez.domain.model.NavigationEntries;

/**
 * Repository taht gets the options to be printed on the Navigation drawer.
 * @author Débora Gómez Bertoli.
 */
public interface MenuOptionsRepository {

    /**
     * Called whe the request finishes.
     */
    interface Callback {

        void onSuccess(@NonNull NavigationEntries entries);

        void onError(@NonNull Exception exception);

    }

    /**
     * Gets the entries for the navigation drawer menu.
     * @param callback the callback.
     */
    void getMenuEntries(@NonNull Callback callback);

}
