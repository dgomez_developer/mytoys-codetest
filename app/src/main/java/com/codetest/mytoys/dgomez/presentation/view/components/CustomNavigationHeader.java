package com.codetest.mytoys.dgomez.presentation.view.components;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codetest.mytoys.dgomez.R;

/**
 * Class that represents the header of the navigation view.
 * @author Débora Gómez Bertoli.
 */
public class CustomNavigationHeader extends LinearLayout {

    private ImageButton backButton;

    private ImageButton closeButton;

    private TextView headerTitle;

    private ImageView headerLogo;

    private HeaderClickListener listener;

    public CustomNavigationHeader(Context context) {
        super(context);
        initViews(context);
    }

    public CustomNavigationHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews(context);
    }

    public CustomNavigationHeader(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews(context);
    }

    private void initViews(Context context) {

        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mInflater.inflate(R.layout.navigation_header, this);

        backButton = (ImageButton) findViewById(R.id.navigation_back_button);
        closeButton = (ImageButton) findViewById(R.id.navigation_close_button);
        headerTitle = (TextView) findViewById(R.id.navigation_header_text);
        headerLogo = (ImageView) findViewById(R.id.navigation_header_logo);
        headerLogo.setVisibility(View.VISIBLE);

        backButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClickBackButton();
                }
            }
        });

        closeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClickCloseButton();
                }
            }
        });
    }

    /**
     * Registers a listener for the header clicks.
     * @param mHeaderListener the listener.
     */
    public void setHeaderListener(@NonNull HeaderClickListener mHeaderListener) {
        this.listener = mHeaderListener;
    }

    /**
     * Sets the header content depending on the options selected.
     * @param title the header title.
     */
    public void setHeaderContent(@NonNull String title) {

        if (TextUtils.isEmpty(title)) {

            headerTitle.setText(R.string.app_name);
            headerTitle.setVisibility(View.GONE);
            backButton.setVisibility(View.GONE);
            headerLogo.setVisibility(View.VISIBLE);

        } else {

            headerTitle.setVisibility(VISIBLE);
            headerTitle.setText(title);
            backButton.setVisibility(View.VISIBLE);
            headerLogo.setVisibility(View.GONE);
        }
    }

    /**
     * Interface for the listener of the header clicks.
     */
    public interface HeaderClickListener {

        /**
         * Called when back button is pressed.
         */
        void onClickBackButton();

        /**
         * Called when close button is closed.
         */
        void onClickCloseButton();
    }
}