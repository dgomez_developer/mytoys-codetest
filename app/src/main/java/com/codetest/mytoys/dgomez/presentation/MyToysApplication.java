package com.codetest.mytoys.dgomez.presentation;

import android.app.Application;

import com.codetest.mytoys.dgomez.presentation.di.components.ApplicationComponent;
import com.codetest.mytoys.dgomez.presentation.di.components.DaggerApplicationComponent;
import com.codetest.mytoys.dgomez.presentation.di.modules.ApplicationModule;

/**
 * Class that represents the application object.
 * @author Débora Gómez Bertoli.
 */
public class MyToysApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initializeInjector();
    }

    private void initializeInjector() {

        this.applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        this.applicationComponent.inject(this);
    }

    public ApplicationComponent getApplicationComponent() {
        return this.applicationComponent;
    }

}
