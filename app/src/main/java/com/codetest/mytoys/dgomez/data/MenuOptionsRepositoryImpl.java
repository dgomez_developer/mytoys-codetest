package com.codetest.mytoys.dgomez.data;

import javax.inject.Inject;
import javax.inject.Named;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.codetest.mytoys.dgomez.data.datasource.MenuOptionsDataSource;
import com.codetest.mytoys.dgomez.domain.model.NavigationEntries;
import com.codetest.mytoys.dgomez.domain.repository.MenuOptionsRepository;

/**
 * Repository for menu options.
 * @author Débora Gómez Bertoli.
 */
public class MenuOptionsRepositoryImpl implements MenuOptionsRepository {

    private MenuOptionsDataSource networkDataSource;

    @Inject
    public MenuOptionsRepositoryImpl(@NonNull @Named("network") MenuOptionsDataSource networkDataSource) {
        this.networkDataSource = networkDataSource;
    }

    @Override
    public void getMenuEntries(@NonNull final Callback callback) {

        networkDataSource.getOptions(new MenuOptionsDataSource.Callback() {
            @Override
            public void onSuccess(@Nullable NavigationEntries entries) {

                if (entries == null || entries.getNavigationEntries() == null || entries.getNavigationEntries().isEmpty()) {

                    callback.onError(new Exception("NO_MENU_OPTIONS_FOUND"));

                } else {

                    callback.onSuccess(entries);
                }

            }

            @Override
            public void onError(@NonNull Exception exception) {

                callback.onError(new Exception("ERROR_GETTING_THE_OPTIONS_FROM_SERVER"));

            }
        });

    }
}
