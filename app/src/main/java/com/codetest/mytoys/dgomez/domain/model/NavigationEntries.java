package com.codetest.mytoys.dgomez.domain.model;

import java.util.ArrayList;
import java.util.List;

/**
 * All the navigation entries of the menu.
 * @author Débora Gómez Bertoli.
 */
public class NavigationEntries {

    private List<NavigationEntry> navigationEntries = new ArrayList<>();

    /**
     * @return The navigationEntries
     */
    public List<NavigationEntry> getNavigationEntries() {
        return navigationEntries;
    }

    /**
     * @param navigationEntries The navigationEntries
     */
    public void setNavigationEntries(List<NavigationEntry> navigationEntries) {
        this.navigationEntries = navigationEntries;
    }

}
