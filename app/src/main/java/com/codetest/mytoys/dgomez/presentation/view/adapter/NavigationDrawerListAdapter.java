package com.codetest.mytoys.dgomez.presentation.view.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.codetest.mytoys.dgomez.R;
import com.codetest.mytoys.dgomez.domain.model.Child;
import com.codetest.mytoys.dgomez.domain.model.NavigationEntry;

/**
 * Adapter for the items in the Navigation Drawer.
 * @author Débora Gómez Bertoli.
 */
public class NavigationDrawerListAdapter extends BaseAdapter {

    private final Context context;

    private ArrayList<Object> items = new ArrayList<>();

    /**
     * Constructor.
     * @param context The context.
     */
    public NavigationDrawerListAdapter(@NonNull Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        if (items != null) {
            return items.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if (items != null) {
            return items.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.navigation_item, null);
        }
        if (items != null) {

            Object items = this.items.get(position);

            if (items != null) {

                fillRow(convertView, items);
            }
        }
        return convertView;
    }

    /**
     * Sets the list of items for the listview.
     * @param list list of items
     */
    public void setList(ArrayList list) {
        items = list;
        notifyDataSetChanged();
    }

    private void fillRow(View convertView, Object actualNavObject) {

        if (actualNavObject instanceof NavigationEntry) {

            NavigationEntry entity = (NavigationEntry) actualNavObject;
            fillInformation(convertView, entity.getLabel(), entity.getChildren().size() > 0, true);

        } else if (actualNavObject instanceof Child) {

            Child entity = (Child) actualNavObject;
            fillInformation(convertView, entity.getLabel(), entity.getChildren() == null, false);
        }
    }

    private void fillInformation(View convertView, String label, boolean isNode, boolean isSection) {

        ((TextView) convertView.findViewById(R.id.navigationLabel)).setText(label);
        if (isNode) {
            convertView.findViewById(R.id.navigationIcon).setVisibility(View.GONE);
        } else {
            convertView.findViewById(R.id.navigationIcon).setVisibility(View.VISIBLE);
        }
        if (isSection) {
            convertView.findViewById(R.id.navigationLayout).setBackgroundColor(Color.LTGRAY);
        } else {
            int[] attrs = new int[]{R.attr.selectableItemBackground};
            TypedArray typedArray = context.obtainStyledAttributes(attrs);
            int backgroundResource = typedArray.getResourceId(0, 0);
            convertView.findViewById(R.id.navigationLayout).setBackgroundResource(backgroundResource);
            typedArray.recycle();
        }
    }
}
