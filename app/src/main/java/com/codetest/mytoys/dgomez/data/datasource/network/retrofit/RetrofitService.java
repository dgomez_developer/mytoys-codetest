package com.codetest.mytoys.dgomez.data.datasource.network.retrofit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * This classs initializes the retrofit service and configures it.
 * @author Débora Gómez Bertoli.
 */
public class RetrofitService {

    private static final String BASE_URL = "https://mytoysiostestcase1.herokuapp.com";

    private static Retrofit buildJsonRetrofitService() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        return new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(JacksonConverterFactory.create())
                .client(client)
                .build();

    }

    /**
     * Creates the JSON API.
     * @return json service instance.
     */
    public static JsonService getJsonService() {

        return buildJsonRetrofitService().create(JsonService.class);
    }

}
