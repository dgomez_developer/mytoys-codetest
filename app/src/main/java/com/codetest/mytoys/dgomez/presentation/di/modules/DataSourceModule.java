package com.codetest.mytoys.dgomez.presentation.di.modules;

import javax.inject.Named;

import com.codetest.mytoys.dgomez.data.datasource.MenuOptionsDataSource;
import com.codetest.mytoys.dgomez.data.datasource.network.MenuOptionsNetworkDataSource;

import dagger.Module;
import dagger.Provides;

/**
 * Data sources dependencies.
 * @author Débora Gómez Bertoli.
 */
@Module(includes = {ApplicationModule.class})
public class DataSourceModule {


    @Provides
    @Named("network")
    public MenuOptionsDataSource provideNetworkDataSource(MenuOptionsNetworkDataSource dataSource){
        return dataSource;
    }

}
