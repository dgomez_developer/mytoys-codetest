package com.codetest.mytoys.dgomez.domain.model;

import java.util.ArrayList;
import java.util.List;

/**
 * A single entry for a menu option.
 * @author Débora Gómez Bertoli.
 */
public class NavigationEntry {

    private String type;

    private String label;

    private List<Child> children = new ArrayList<>();

    /**
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The label
     */
    public String getLabel() {
        return label;
    }

    /**
     * @param label The label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * @return The children
     */
    public List<Child> getChildren() {
        return children;
    }

    /**
     * @param children The children
     */
    public void setChildren(List<Child> children) {
        this.children = children;
    }

}
