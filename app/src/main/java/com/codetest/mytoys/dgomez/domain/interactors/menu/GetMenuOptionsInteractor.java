package com.codetest.mytoys.dgomez.domain.interactors.menu;

import android.support.annotation.NonNull;

import com.codetest.mytoys.dgomez.domain.model.NavigationEntries;

/**
 * Gets the entries that should be showed on the navigation drawer.
 * @author Débora Gómez Bertoli.
 */
public interface GetMenuOptionsInteractor {

    /**
     * Called when the request finishes.
     */
    interface Callback {

        /**
         * Gets the menu entries.
         * @param entries the entries.
         */
        void onSuccess(@NonNull NavigationEntries entries);

        /**
         * Called when an error happens
         * @param e the error.
         */
        void onError(@NonNull Exception e);

    }

    /**
     * Gets the menu options.
     * @param callback the callback.
     */
    void execute(@NonNull Callback callback);

}
