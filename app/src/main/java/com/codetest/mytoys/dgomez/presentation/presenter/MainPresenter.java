package com.codetest.mytoys.dgomez.presentation.presenter;

import android.support.annotation.NonNull;

import com.codetest.mytoys.dgomez.presentation.view.MainView;

/**
 * Presenter for the @link{MainActivity.java}
 * @author Débora Gómez Bertoli.
 */
public interface MainPresenter {

    /**
     * Performs the onCreate operations.
     */
    void onCreate();

    /**
     * Links the presenter to the view.
     * @param view the view.
     */
    void setView(@NonNull MainView view);

}
