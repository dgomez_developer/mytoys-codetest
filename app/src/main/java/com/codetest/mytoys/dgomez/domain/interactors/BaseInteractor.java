package com.codetest.mytoys.dgomez.domain.interactors;

import com.codetest.mytoys.dgomez.domain.executors.PostExecutionThread;
import com.codetest.mytoys.dgomez.domain.executors.ThreadExecutor;

/**
 * Base interactor that executes a task in background thread and post the result to a callback that will be executed in
 * the UI thread.
 * @author Débora Gómez Bertoli.
 */
public abstract class BaseInteractor implements Interactor {

    private final ThreadExecutor threadExecutor;

    private final PostExecutionThread postExecutionThread;

    /**
     * Constructor.
     * @param threadExecutor The thread executor.
     * @param postExecutionThread The post execution thread.
     */
    public BaseInteractor(ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        this.threadExecutor = threadExecutor;
        this.postExecutionThread = postExecutionThread;
    }

    /**
     * Executes the operation in background.
     */
    protected void execute() {
        threadExecutor.execute(this);
    }

    /**
     * Returns the result of the operation on the UI thread.
     * @param runnable The UI runnable.
     */
    protected void post(Runnable runnable) {
        postExecutionThread.post(runnable);
    }
}
