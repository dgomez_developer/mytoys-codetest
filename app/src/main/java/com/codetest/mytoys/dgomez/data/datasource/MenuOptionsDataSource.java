package com.codetest.mytoys.dgomez.data.datasource;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.codetest.mytoys.dgomez.domain.model.NavigationEntries;

/**
 * Data source for getting the opstions information.
 * @author Débora Gómez Bertoli.
 */
public interface MenuOptionsDataSource {

    /**
     * Called when the request finishes.
     */
    interface Callback {

        /**
         * Gets the entries.
         * @param entries the menu entries.
         */
        void onSuccess(@Nullable NavigationEntries entries);

        /**
         * Called when an error happens.
         * @param exception the error.
         */
        void onError(@NonNull Exception exception);

    }

    /**
     * Gets the menu options.
     * @param callback the callback.
     */
    void getOptions(@NonNull Callback callback);

}
