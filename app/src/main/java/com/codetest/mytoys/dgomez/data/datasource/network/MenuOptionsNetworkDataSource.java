package com.codetest.mytoys.dgomez.data.datasource.network;

import java.io.IOException;

import javax.inject.Inject;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.util.Log;

import com.codetest.mytoys.dgomez.data.datasource.MenuOptionsDataSource;
import com.codetest.mytoys.dgomez.data.datasource.network.retrofit.JsonService;
import com.codetest.mytoys.dgomez.domain.model.NavigationEntries;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Gets the menu options from the backend.
 * @author Débora Gómez Bertoli.
 */
public class MenuOptionsNetworkDataSource implements MenuOptionsDataSource {

    private static final String TAG = "NetworkDataSource";

    private Context context;

    private JsonService jsonService;

    @Inject
    public MenuOptionsNetworkDataSource(@NonNull JsonService jsonService, @NonNull Context context) {
        this.context = context;
        this.jsonService = jsonService;
    }

    @Override
    public void getOptions(@NonNull Callback callback) {

        if (!isOnline()) {
            callback.onError(new Exception("NO_NETWORK_CONNECTION"));
            return;
        }

        Call<NavigationEntries> entries = jsonService.getEntries();
        try {
            Response<NavigationEntries> response = entries.execute();
            ResponseBody errorBody = response.errorBody();
            if (errorBody == null) {

                callback.onSuccess(response.body());

            } else {

                Log.e(TAG, errorBody.string());
                callback.onError(new Exception("ERROR IN REQUEST"));
            }
        } catch (IOException e) {
            callback.onError(e);
            Log.e(TAG, e.toString());
        }

    }

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
