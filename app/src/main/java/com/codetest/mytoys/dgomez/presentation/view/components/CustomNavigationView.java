package com.codetest.mytoys.dgomez.presentation.view.components;

import java.util.ArrayList;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.codetest.mytoys.dgomez.R;
import com.codetest.mytoys.dgomez.domain.model.Child;
import com.codetest.mytoys.dgomez.domain.model.NavigationEntries;
import com.codetest.mytoys.dgomez.domain.model.NavigationEntry;
import com.codetest.mytoys.dgomez.presentation.view.adapter.NavigationDrawerListAdapter;

/**
 * Class that represents the navigation view of the navigation drawer.
 * @author Débora Gómez Bertoli.
 */
public class CustomNavigationView extends LinearLayout {

    private ListView listView;

    private NavigationDrawerListAdapter adapter;

    private CustomNavigationHeader navigationHeader;

    private NavigationDrawerItemListener backListener;

    private ArrayList<Object> historyNavigation;

    private NavigationEntries baseNavEntry;

    public CustomNavigationView(Context context) {
        super(context);
        initViews(context);
    }

    public CustomNavigationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews(context);
    }

    public CustomNavigationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews(context);
    }

    private void initViews(Context context) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.navigation_drawer, this);

        historyNavigation = new ArrayList<>();

        initList(context);

        initHeader(context);

        listView.addHeaderView(navigationHeader);
    }

    private void initHeader(Context context) {
        navigationHeader = new CustomNavigationHeader(context);
        navigationHeader.setHeaderContent("");
        navigationHeader.setHeaderListener(new CustomNavigationHeader.HeaderClickListener() {
            @Override
            public void onClickBackButton() {
                navigateBack();
            }

            @Override
            public void onClickCloseButton() {
                if (backListener != null) {
                    backListener.onBackPressed();
                }

            }
        });
    }

    private void initList(Context context) {
        adapter = new NavigationDrawerListAdapter(context);
        listView = (ListView) findViewById(R.id.navigationList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                manageClick(position - 1);
            }
        });
    }

    private void navigateBack() {

        if (historyNavigation.size() > 1) {
            historyNavigation.remove(historyNavigation.size() - 1);
            Object lastChild = historyNavigation.get(historyNavigation.size() - 1);
            navigateToChild(lastChild);
            if (lastChild instanceof Child) {
                navigationHeader.setHeaderContent(((Child) lastChild).getLabel());
            } else {
                navigationHeader.setHeaderContent(((NavigationEntry) lastChild).getLabel());
            }
        } else {
            historyNavigation.clear();
            fillNavigationItems(baseNavEntry);
            navigationHeader.setHeaderContent("");
        }
    }

    private void manageClick(int position) {

        Object selectedItem = adapter.getItem(position);

        if (selectedItem instanceof Child) {

            Child item = (Child) selectedItem;

            if (!TextUtils.isEmpty(item.getUrl())) {

                manageLinkClick(item);

            } else {

                manageNodeClick(item);
            }
        } else if (selectedItem instanceof NavigationEntry) {

            manageSectionClick((NavigationEntry) selectedItem);
        }
    }

    private void manageNodeClick(Child item) {
        if (!item.getChildren().isEmpty()) {
            historyNavigation.add(item);
            navigateToChild(item);
        }
    }

    private void manageLinkClick(Child item) {
        if (backListener != null) {
            backListener.loadUrl(item.getUrl());
        }
    }

    private void manageSectionClick(NavigationEntry item) {

        if (!item.getChildren().isEmpty()) {
            historyNavigation.add(item);
            navigateToChild(item);
        }
    }

    private void navigateToChild(Object item) {

        if (item instanceof Child) {

            navigationHeader.setHeaderContent(((Child) item).getLabel());
            adapter.setList(new ArrayList<>(((Child) item).getChildren()));

        } else {

            navigationHeader.setHeaderContent(((NavigationEntry) item).getLabel());
            adapter.setList(new ArrayList<>(((NavigationEntry) item).getChildren()));
        }
    }

    private void fillNavigationItems(NavigationEntries entries) {

        ArrayList initList = new ArrayList();

        if ((entries != null) && (entries.getNavigationEntries() != null)) {

            for (NavigationEntry entity : entries.getNavigationEntries()) {

                initList.add(entity);
                if (entity.getChildren() != null) {
                    for (Child child : entity.getChildren()) {
                        initList.add(child);
                    }

                }
            }
        }
        adapter.setList(initList);
    }

    /**
     * Init the component with the data.
     * @param entries list of entries in the navigation drawer.
     */
    public void initListViewContent(NavigationEntries entries) {

        baseNavEntry = entries;
        fillNavigationItems(entries);

    }

    /**
     * Listener for back event.
     * @param backListener the listener.
     */
    public void setBackListener(NavigationDrawerItemListener backListener) {
        this.backListener = backListener;
    }

    /**
     * Interface for navigation view clicks.
     */
    public interface NavigationDrawerItemListener {

        /**
         * Called when header back button is pressed
         */
        void onBackPressed();

        /**
         * Called when a link item is clicked.
         * @param url the URL.
         */
        void loadUrl(@NonNull String url);
    }
}
