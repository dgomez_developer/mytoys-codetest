package com.codetest.mytoys.dgomez.presentation.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import com.codetest.mytoys.dgomez.R;
import com.github.rahatarmanahmed.cpv.CircularProgressView;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;

/**
 * Class that loads a WebView on the screen.
 * @author Débora Gómez Bertoli.
 */
public class WebViewFragment extends Fragment {

    private static final String HOME_URL = "https://www.mytoys.de";

    @BindView(R.id.main_webview)
    WebView webview;

    @BindView(R.id.main_progressbar)
    CircularProgressView progressBar;

    @BindView(R.id.main_progressbar_layout)
    LinearLayout progressBarLayout;

    @BindView(R.id.main_refresh)
    PullToRefreshLayout refreshLayout;

    private String currentUrl;

    /**
     * Constructor.
     */
    public WebViewFragment() {

    }

    /**
     * Creates an instance of the webview.
     * @return the web view fragment.
     */
    public static WebViewFragment newInstance() {
        WebViewFragment fragment = new WebViewFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_webview, container, false);
        ButterKnife.bind(this, view);
        initWebView();
        initPullToRefresh();
        loadUrl(HOME_URL);
        return view;
    }

    private void initPullToRefresh() {
        ActionBarPullToRefresh.from(getActivity())
                .allChildrenArePullable()
                .listener(new OnRefreshListener() {
                    @Override
                    public void onRefreshStarted(View view) {
                        loadUrlWithoutAnimation(currentUrl);
                        refreshLayout.setRefreshComplete();
                    }
                })
                .setup(refreshLayout);
    }

    private void initWebView() {

        webview.getSettings().setJavaScriptEnabled(true);
        webview.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                progressBar.stopAnimation();
                progressBarLayout.setVisibility(View.GONE);
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Snackbar.make(getView(), description, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Loads a new URL on the webview.
     * @param url the URL.
     */
    public void loadUrl(@NonNull String url) {
        progressBarLayout.setVisibility(View.VISIBLE);
        progressBar.startAnimation();
        currentUrl = url;
        webview.loadUrl(url);
    }

    private void loadUrlWithoutAnimation(String url) {
        currentUrl = url;
        webview.loadUrl(url);
    }

}
