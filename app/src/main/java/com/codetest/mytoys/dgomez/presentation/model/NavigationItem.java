package com.codetest.mytoys.dgomez.presentation.model;

import java.util.List;

/**
 * Entity for navigation drawer items.
 * @author Débora Gómez Bertoli.
 */
public class NavigationItem {

    /**
     * Types of Navigation Item.
     */
    public enum Type {

        SECTION,
        NODE,
        LINK

    }

    private String url;

    private String label;

    private Type type;

    private List<NavigationItem> childItems;

    /**
     * Constructor.
     * @param url
     * @param label
     * @param type
     * @param childItems
     */
    public NavigationItem(String url, String label, Type type, List<NavigationItem> childItems) {
        this.url = url;
        this.label = label;
        this.type = type;
        this.childItems = childItems;
    }

    public Type getType() {
        return type;
    }

    public String getLabel() {
        return label;
    }

    public String getUrl() {
        return url;
    }

    public List<NavigationItem> getChildItems() {
        return childItems;
    }
}
