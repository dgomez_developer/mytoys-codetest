package com.codetest.mytoys.dgomez.presentation.presenter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.codetest.mytoys.dgomez.domain.interactors.menu.GetMenuOptionsInteractor;
import com.codetest.mytoys.dgomez.domain.interactors.menu.GetMenuOptionsInteractor.Callback;
import com.codetest.mytoys.dgomez.domain.model.NavigationEntries;
import com.codetest.mytoys.dgomez.presentation.view.MainView;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

/**
 * Tests for class {@link MainPresenterImpl}
 * @author Débora Gómez Bertoli.
 */
public class MainPresenterImplTest {

    private MainPresenterImpl presenterUnderTest;

    private GetMenuOptionsInteractor mockInteractor;

    private MainView mockView;

    @Before
    public void setup() {
        this.mockView = mock(MainView.class);
        this.mockInteractor = mock(GetMenuOptionsInteractor.class);
        presenterUnderTest = new MainPresenterImpl(mockInteractor);
        presenterUnderTest.setView(mockView);
    }

    @Test
    public void whenGettingMenuOptionsThenOptionsArePresented() {

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback callback = (Callback) invocation.getArguments()[0];
                callback.onSuccess(mock(NavigationEntries.class));
                return null;
            }
        }).when(mockInteractor).execute(any(Callback.class));

        presenterUnderTest.onCreate();

        verify(mockView).fillNavigationDrawer(any(NavigationEntries.class));
        verify(mockView, never()).showError();
    }

    @Test
    public void whenGettingMenuOptionsThenAnErrorOccurs() {

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback callback = (Callback) invocation.getArguments()[0];
                callback.onError(mock(Exception.class));
                return null;
            }
        }).when(mockInteractor).execute(any(Callback.class));

        presenterUnderTest.onCreate();

        verify(mockView).showError();
        verify(mockView, never()).fillNavigationDrawer(any(NavigationEntries.class));

    }

}
