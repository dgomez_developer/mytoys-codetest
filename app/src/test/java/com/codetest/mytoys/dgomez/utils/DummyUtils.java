package com.codetest.mytoys.dgomez.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

import com.codetest.mytoys.dgomez.domain.model.NavigationEntries;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Class for creating dummy object for unit tests.
 * @author Débora Gómez Bertoli.
 */
public class DummyUtils {

    private static final String JSON_PATH = "./src/test/resources/json";

    /**
     * Generates a dummy object with the server response.
     * @return the navigation entries.
     */
    public static NavigationEntries createDummyNavigationEntries() {

        String jsonResponse = getStringFromFile("get_menu_options.json");

        ObjectMapper mapper = new ObjectMapper();

        try {
            return mapper.readValue(jsonResponse, NavigationEntries.class);
        } catch (IOException e) {
            return null;
        }
    }

    /**
     * Returns the contents of the file.
     * @param jsonFileName file name in the JSON_PATH folder.
     * @return The contests of the file as a String.
     */
    public static String getStringFromFile(String jsonFileName) {
        return getStringFromFile(new File(JSON_PATH, jsonFileName));

    }

    private static String getStringFromFile(File jsonFile) {
        try {
            if (jsonFile == null) {
                return "";
            }
            return new Scanner(jsonFile, "UTF-8").useDelimiter("\\A").next();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

}
