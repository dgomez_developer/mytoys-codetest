package com.codetest.mytoys.dgomez.utils;

import com.codetest.mytoys.dgomez.domain.executors.PostExecutionThread;
import com.codetest.mytoys.dgomez.domain.executors.ThreadExecutor;

/**
 * This class is intended to be used for unit tests to run the logic in the same thread and checks results.
 * @author Débora Gómez Bertoli.
 */
public class SameThreadExecutor implements ThreadExecutor, PostExecutionThread {

    @Override
    public void execute(Runnable command) {
        command.run();
    }

    @Override
    public void post(Runnable runnable) {
        runnable.run();
    }
}
