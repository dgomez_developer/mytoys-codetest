package com.codetest.mytoys.dgomez.domain.interactors;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.codetest.mytoys.dgomez.domain.interactors.menu.GetMenuOptionsInteractor;
import com.codetest.mytoys.dgomez.domain.interactors.menu.GetMenuOptionsInteractorImpl;
import com.codetest.mytoys.dgomez.domain.model.NavigationEntries;
import com.codetest.mytoys.dgomez.domain.repository.MenuOptionsRepository;
import com.codetest.mytoys.dgomez.domain.repository.MenuOptionsRepository.Callback;
import com.codetest.mytoys.dgomez.utils.DummyUtils;
import com.codetest.mytoys.dgomez.utils.SameThreadExecutor;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

/**
 * Tests for class {@link com.codetest.mytoys.dgomez.domain.interactors.menu.GetMenuOptionsInteractorImpl}
 * @author Débora Gómez Bertoli.
 */
public class GetMenuOptionsInteractorImplTest {


    private GetMenuOptionsInteractorImpl interactorUnderTest;

    private MenuOptionsRepository mockRepository;

    private GetMenuOptionsInteractor.Callback mockCallback;

    @Before
    public void setup(){
        this.mockCallback = mock(GetMenuOptionsInteractor.Callback.class);
        this.mockRepository = mock(MenuOptionsRepository.class);
        SameThreadExecutor executor = new SameThreadExecutor();
        interactorUnderTest = new GetMenuOptionsInteractorImpl(executor,executor,mockRepository);
    }


    @Test
    public void whenGettingMenuOptionsThenOptionsAreRetrieved(){

        final NavigationEntries dummyEntries = DummyUtils.createDummyNavigationEntries();

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback callback = (Callback) invocation.getArguments()[0];
                callback.onSuccess(dummyEntries);
                return null;
            }
        }).when(mockRepository).getMenuEntries(any(Callback.class));

        interactorUnderTest.execute(mockCallback);

        verify(mockCallback).onSuccess(eq(dummyEntries));
        verify(mockCallback, never()).onError(any(Exception.class));
    }

    @Test
    public void whenGettingMenuOptionsAndAnErrorOccursTheOnErrorIsCalled(){

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback callback = (Callback) invocation.getArguments()[0];
                callback.onError(mock(Exception.class));
                return null;
            }
        }).when(mockRepository).getMenuEntries(any(Callback.class));

        interactorUnderTest.execute(mockCallback);

        verify(mockCallback, never()).onSuccess(any(NavigationEntries.class));
        verify(mockCallback).onError(any(Exception.class));

    }

}
