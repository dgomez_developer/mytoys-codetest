package com.codetest.mytoys.dgomez.data;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.codetest.mytoys.dgomez.data.datasource.MenuOptionsDataSource;
import com.codetest.mytoys.dgomez.data.datasource.MenuOptionsDataSource.Callback;
import com.codetest.mytoys.dgomez.domain.model.NavigationEntries;
import com.codetest.mytoys.dgomez.domain.model.NavigationEntry;
import com.codetest.mytoys.dgomez.domain.repository.MenuOptionsRepository;
import com.codetest.mytoys.dgomez.utils.DummyUtils;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

/**
 * Tests for class {@link MenuOptionsRepositoryImpl}
 * @author Débora Gómez Bertoli.
 */
public class MenuOptionsRepositoryImplTest {

    private MenuOptionsRepositoryImpl repositoryUnderTest;

    private MenuOptionsDataSource mockDataSource;

    private MenuOptionsRepository.Callback mockCallback;

    @Before
    public void setup() {
        this.mockCallback = mock(MenuOptionsRepository.Callback.class);
        this.mockDataSource = mock(MenuOptionsDataSource.class);
        repositoryUnderTest = new MenuOptionsRepositoryImpl(mockDataSource);
    }

    @Test
    public void whenGettingMenuOptionsThenNoErrorOccurs() {

        final NavigationEntries dummyEntries = DummyUtils.createDummyNavigationEntries();

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback callback = (Callback) invocation.getArguments()[0];
                callback.onSuccess(dummyEntries);
                return null;
            }
        }).when(mockDataSource).getOptions(any(Callback.class));

        repositoryUnderTest.getMenuEntries(mockCallback);

        verify(mockCallback).onSuccess(eq(dummyEntries));
        verify(mockCallback, never()).onError(any(Exception.class));

    }

    @Test
    public void whenGettingMenuOptionsAndNoOptionsAreRetrievedThenOnErrorIsCalled() {

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback callback = (Callback) invocation.getArguments()[0];
                callback.onSuccess(null);
                return null;
            }
        }).when(mockDataSource).getOptions(any(Callback.class));

        repositoryUnderTest.getMenuEntries(mockCallback);

        verify(mockCallback, never()).onSuccess(any(NavigationEntries.class));
        verify(mockCallback).onError(any(Exception.class));

    }

    @Test
    public void whenGettingMenuOptionsAndNoChildOptionsAreRetrievedThenOnErrorIsCalled() {

        final NavigationEntries dummyEntries = DummyUtils.createDummyNavigationEntries();
        dummyEntries.setNavigationEntries(null);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback callback = (Callback) invocation.getArguments()[0];
                callback.onSuccess(dummyEntries);
                return null;
            }
        }).when(mockDataSource).getOptions(any(Callback.class));

        repositoryUnderTest.getMenuEntries(mockCallback);

        verify(mockCallback, never()).onSuccess(any(NavigationEntries.class));
        verify(mockCallback).onError(any(Exception.class));

    }

    @Test
    public void whenGettingMenuOptionsAndEmptyChildOptionsAreRetrievedThenOnErrorIsCalled() {

        final NavigationEntries dummyEntries = DummyUtils.createDummyNavigationEntries();
        dummyEntries.setNavigationEntries(new ArrayList<NavigationEntry>());

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback callback = (Callback) invocation.getArguments()[0];
                callback.onSuccess(dummyEntries);
                return null;
            }
        }).when(mockDataSource).getOptions(any(Callback.class));

        repositoryUnderTest.getMenuEntries(mockCallback);

        verify(mockCallback, never()).onSuccess(any(NavigationEntries.class));
        verify(mockCallback).onError(any(Exception.class));

    }

    @Test
    public void whenGettingMenuOptionsThenAnErrorOccurs() {

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback callback = (Callback) invocation.getArguments()[0];
                callback.onError(mock(Exception.class));
                return null;
            }
        }).when(mockDataSource).getOptions(any(Callback.class));

        repositoryUnderTest.getMenuEntries(mockCallback);

        verify(mockCallback, never()).onSuccess(any(NavigationEntries.class));
        verify(mockCallback).onError(any(Exception.class));

    }

}
