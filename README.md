Android project developed as a PoC for My Toys recruiting project.

**App Architecture**

The architecture follows the _Clean Architecture principles_ where each layer is isolated from the others. For more information regarding this type of architecture please visit [Clean Architecture Post](https://8thlight.com/blog/uncle-bob/2012/08/13/the-clean-architecture.html)

The application is divided in 3 main blocks:

- **Presentation**: All the view and its adapter are located here.  
- **Domain**: All the business logic rules are located here.
- **Data**: Acts as a repository for the information needed in the application. It only holds a netwrok interface but the idea is that a local storage or a caché can be added in this module.

**Libraries**

This projects includes som third party libraries to improve the developer expirience:

- ButterKnife: for binding view from `Fragment`.
- Retrofit: for network communications.
- Dagger: for dependency injection.
- ActionPullToRefresh from _Chris Banes_: for refreshing the `WebView`content.
- CircularProgressiveView from _Rahatarma Ahmed_: for circular material progress view compatibility.

**Unit Tests**

Under the root `src/test/java` you can find unit test for all the layers. `Mockito`and `JUnit`libraries have been used in order to implement the tests.

**RxJava PoC**
The repository branch `rxjava_poc`holds a version of the application using RxJava + Retrofit for network operations.